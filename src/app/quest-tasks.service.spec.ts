import { TestBed, inject } from '@angular/core/testing';

import { QuestTasksService } from './quest-tasks.service';

describe('QuestTasksService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QuestTasksService]
    });
  });

  it('should be created', inject([QuestTasksService], (service: QuestTasksService) => {
    expect(service).toBeTruthy();
  }));
});
