import { Injectable } from '@angular/core';

@Injectable()
export class QuestTasksService {

  constructor() { }
  public tasks = [
    {
      id: '1',
      image: 'http://lorempixel.com/g/400/200/?q=1',
      answer: '',
      min: 0,
      sec: 0
    },
    {
      id: '2',
      image: 'http://lorempixel.com/g/400/200/?q=2',
      answer: '',
      min: 0,
      sec: 0
    },
    {
      id: '3',
      image: 'http://lorempixel.com/g/400/200/?q=3',
      answer: '',
      min: 0,
      sec: 0
    },
  ];

  public fetchTasks = () => Promise.resolve(this.tasks);

  public answered = [];

  public postAnswer = ({ id, answer }) => new Promise((resolve, reject) => {
    console.log({ id, answer });
    if (this.answered.includes(id)) return reject(new Error('already answered'));
    if (!Array.from('123').includes(id)) return reject(new Error('wrong id'));
    this.answered.push({
      id, answer
    });
    return resolve(`answer ${answer} accepted`);
  });

}
