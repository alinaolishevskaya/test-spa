import { Component, OnInit } from '@angular/core';
import {QuestTasksService} from '../quest-tasks.service';
import {trigger, style, animate, transition} from '@angular/animations';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css'],
  animations: [
    trigger('fadeInOut', [
      transition(':enter', [
        style({opacity: 0}),
        animate(500, style({opacity: 1}))
      ]),
      transition(':leave', [
        animate(500, style({opacity: 0}))
      ])
    ])
  ]
})
export class TasksComponent implements OnInit {

  constructor( private questTasksService: QuestTasksService) { }

  public tasks = this.questTasksService.tasks;
  public activeId: number;
  public show: boolean = false;
  public showNext: boolean = true;
  public showTasks: boolean = false;
  public sec: any;
  public min: any;


  ngOnInit() {
    this.activeId = 0;
    setInterval(() => {
      this.timePage();
    }, 1000);
  }

  public nextItem() {
    let maxId = this.tasks.length - 1;
    this.activeId++;
    this.show = true;
    if (this.activeId >= maxId) {
      this.showNext = false;
      this.activeId = maxId;
    }
    //this.timePage();
  }

  public prevItem() {
    this.activeId--;
    this.show = true;
    if (this.activeId <= 0) {
      this.activeId = 0;
      this.show = false;
      this.showNext = true;
    }
  }

  public answeredTask() {
    let id = this.tasks[this.activeId].id;
    let answer = this.tasks[this.activeId].answer;
    this.show = true;
    if (this.activeId <= 0) {
      this.show = false;
      this.showNext = true;
    }
    if (this.tasks.length > 0) {
      this.questTasksService.postAnswer({id, answer});
      this.tasks.splice(this.activeId, 1);
    }
    if (this.tasks.length == 1) {
      this.showNext = false;
      this.show = false;
    }
    if (this.tasks.length == 0) {
      alert('конец')
    }
    if (!(this.tasks[this.activeId])) {
      this.activeId--;
    }
    let maxIdAfterSplice = this.tasks.length - 1;
    if (this.activeId == maxIdAfterSplice) {
      this.showNext = false;
    }
  }

  public timePage() {
    if (this.tasks.length > 0) {
      this.sec = this.tasks[this.activeId].sec;
      this.min = this.tasks[this.activeId].min;
      this.sec++;
      if (this.sec < 10) {
        this.sec = '0' + this.sec;
      }
      if (this.sec == 60) {
        this.min++;
        this.sec = '0' + 0;
      }
      this.tasks[this.activeId].sec = this.sec;
      this.tasks[this.activeId].min = this.min;
    }
  }
}
