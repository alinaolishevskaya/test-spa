const tasks = [
  {
    id: '1',
    image: 'http://lorempixel.com/g/400/200/?q=1',
  },
  {
    id: '2',
    image: 'http://lorempixel.com/g/400/200/?q=2',
  },
  {
    id: '3',
    image: 'http://lorempixel.com/g/400/200/?q=3',
  },
];

export const fetchTasks = () => Promise.resolve(tasks);

const answered = [];

export const postAnswer = ({ id, answer }) => new Promise((resolve, reject) => {
  console.log({ id, answer });
  if (answered.includes(id)) return reject(new Error('already answered'));
  if (!Array.from('123').includes(id)) return reject(new Error('wrong id'));
  answered.push(id);
  return resolve(`answer ${answer} accepted`);
});

